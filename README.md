# The Bol Game

the source contains 3 packages as follows:
- cli    -> the command interface implementation of the game
- game   -> the core game logic 
- web    -> webapp implementation of the game

## to start the game in web mode

- `git clone https://gitlab.com/ec8_4b4/the-bol-game.git`
- `cd the-bol-game`
- linux: `./gradlew bootJar`  windows: `gradlew bootJar`
- `java -jar build/libs/the-bol-game-1.0.jar`
- open http://localhost:8080

## to start the game in cli mode

- `git clone https://gitlab.com/ec8_4b4/the-bol-game.git`
- `cd the-bol-game`
- linux: `./gradlew bootJar` windows: `gradlew bootJar`
- `java -jar build/libs/the-bol-game-1.0.jar cli`

### to run the test 
linux: `./gradlew check` windows: `gradlew check`

