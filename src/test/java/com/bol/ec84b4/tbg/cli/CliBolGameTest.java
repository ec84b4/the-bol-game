package com.bol.ec84b4.tbg.cli;

import com.bol.ec84b4.tbg.game.Player;
import com.bol.ec84b4.tbg.game.State;
import com.bol.ec84b4.tbg.game.TheBolGame;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class CliBolGameTest {
    private Random rand = new Random(5);

    @Test
    void start() {
        TheBolGame game = new TheBolGame(new State(6, 6, 1),
                new Player[]{new CliPlayer(0), new CliPlayer(1)});

        Scanner in = new Scanner(new InputStreamReader(new InputStream() {
            int lineBreak = '\n';
            int next = lineBreak;

            @Override
            public int read() {
                if (next == lineBreak)
                    next = String.valueOf(rand.nextInt(6)).getBytes()[0];
                else
                    next = lineBreak;

                return next;
            }
        }));

        PrintStream out = new PrintStream(new OutputStream() {
            @Override
            public void write(int i) {
            }
        });

        CliBolGame cliBolGame = new CliBolGame(in, out, game);

        int winner = cliBolGame.start();

        Assertions.assertEquals(0, winner);
    }
}