package com.bol.ec84b4.tbg.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;

class TheBolGameTest {
    private Random rand = new Random(3);

    @Test
    void instantiateGame() {
        TheBolGame theBolGame = newGame();

        Assertions.assertNotNull(theBolGame);
    }

    @Test
    void startGame() {
        TheBolGame theBolGame = newGame();

        int winner = theBolGame.start();

        Assertions.assertEquals(1, winner);
    }

    private TheBolGame newGame() {
        State state = new State(6, 6, 1);
        return new TheBolGame(state, new Player[]{new RandomPlayer(), new RandomPlayer()});
    }

    private class RandomPlayer implements Player {

        @Override
        public int choosePit() {
            return rand.nextInt(6);
        }
    }
}