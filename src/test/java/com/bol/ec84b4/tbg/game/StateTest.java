package com.bol.ec84b4.tbg.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

class StateTest {

    @BeforeEach
    void setup() {

    }

    /**
     * checks the initial state of the state class
     */
    @Test
    void instantiateState() {
        int pitCount = 6, stonePerPit = pitCount;

        State state = new State(pitCount, stonePerPit, 1);

        verifyState(state, 0, 0,
                new int[]{6, 6, 6, 6, 6, 6},
                new int[]{6, 6, 6, 6, 6, 6});
    }

    /**
     * test each player doing 1 turn
     */
    @Test
    void firstCompleteTurn() {
        State state = new State(6, 6, 1);

        Assertions.assertFalse(state.turn(2));//p2

        verifyState(state, 0, 1, new int[]{6, 6, 6, 6, 6, 6}, new int[]{7, 7, 0, 7, 7, 7});

        Assertions.assertFalse(state.turn(3));//p1

        verifyState(state, 1, 1, new int[]{7, 7, 7, 0, 7, 7}, new int[]{7, 7, 0, 7, 7, 7});
    }

    /**
     * test each player doing 2 turns
     */
    @Test
    void secondCompleteTurn() {
        State state = new State(6, 6, 1);

        Assertions.assertFalse(state.turn(2));//p2
        Assertions.assertFalse(state.turn(3));//p1

        Assertions.assertFalse(state.turn(4));//p2

        verifyState(state, 1, 10, new int[]{7, 0, 7, 0, 7, 7}, new int[]{8, 8, 1, 8, 0, 8});

        Assertions.assertFalse(state.turn(5));//p1

        verifyState(state, 11, 10, new int[]{8, 1, 8, 1, 8, 0}, new int[]{0, 8, 1, 8, 0, 8});
    }

    @Test
    void finishGame() {
        State state = new State(6, 6, 1);

        Random rand = new Random(8);

        int turnCounter = 0;
        while (true) {
            boolean finished = state.turn(rand.nextInt(6));

            if (finished) {
                Assertions.assertEquals(0, state.winner());
                break;
            }

            turnCounter++;
        }

        Assertions.assertEquals(52, turnCounter);
    }

    private void verifyState(State state, int p1Vault, int p2Vault, int[] p1Pits, int[] p2Pits) {
        State.Data p1 = state.player1();
        State.Data p2 = state.player2();

        Assertions.assertEquals(p1Vault, p1.vault());
        Assertions.assertEquals(p2Vault, p2.vault());
        Assertions.assertArrayEquals(p1Pits, p1.pits());
        Assertions.assertArrayEquals(p2Pits, p2.pits());
    }

}