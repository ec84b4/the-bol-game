<%@ page import="com.bol.ec84b4.tbg.game.TheBolGame" %>
<%@ page import="com.bol.ec84b4.tbg.web.WebPlayer" %>
<%@ page import="com.bol.ec84b4.tbg.game.State" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
    <title>The Bol Game</title>
    <style>
        th, td {
            border: 1px solid black;
            padding: 10px;
        }
    </style>
</head>
<body>
<div>
    <div>
        <h1>The Bol Game</h1>
        <%
            TheBolGame game = (TheBolGame) request.getAttribute("game");
            State.Data p1 = game.state().player1();
            State.Data p2 = game.state().player2();

            String pit = request.getParameter("pit");

            if (pit != null) {
                WebPlayer player = (WebPlayer) game.players()[game.state().currentPlayer()];

                player.pick(Integer.parseInt(pit));
            }
        %>

        Player 1
        <table>
            <tr>
                <th>Vault</th>
                <th>Pit#5</th>
                <th>Pit#4</th>
                <th>Pit#3</th>
                <th>Pit#2</th>
                <th>Pit#1</th>
                <th>Pit#0</th>
            </tr>
            <tr>
                <td id="p1v"><%=p1.vault()%>
                </td>
                <td id="p0p5"><%=p1.pits()[5]%>
                </td>
                <td id="p0p4"><%=p1.pits()[4]%>
                </td>
                <td id="p0p3"><%=p1.pits()[3]%>
                </td>
                <td id="p0p2"><%=p1.pits()[2]%>
                </td>
                <td id="p0p1"><%=p1.pits()[1]%>
                </td>
                <td id="p0p0"><%=p1.pits()[0]%>
                </td>
            </tr>
        </table>

        <br/>

        <table>
            <tr>
                <td id="p1p0"><%=p2.pits()[0]%>
                </td>
                <td id="p1p1"><%=p2.pits()[1]%>
                </td>
                <td id="p1p2"><%=p2.pits()[2]%>
                </td>
                <td id="p1p3"><%=p2.pits()[3]%>
                </td>
                <td id="p1p4"><%=p2.pits()[4]%>
                </td>
                <td id="p1p5"><%=p2.pits()[5]%>
                </td>
                <td id="p2v"><%=p2.vault()%>
                </td>
            </tr>
            <tr>
                <th>Pit#0</th>
                <th>Pit#1</th>
                <th>Pit#2</th>
                <th>Pit#3</th>
                <th>Pit#4</th>
                <th>Pit#5</th>
                <th>Vault</th>
            </tr>
        </table>
        Player 2

        <br/>
        <br/>
        <br/>

        Player <%=game.state().currentPlayer() + 1%> Turn

        <br/>

        <form action="#" method="post">
            <label>
                Chosen Pit: <input type="number" maxlength="1" min="0" max="5" name="pit"/>
                <input type="hidden" name="t" value="<%=System.currentTimeMillis()%>"/>
                <input type="submit" value="Submit"/>
            </label>
        </form>
    </div>
</div>
</body>
</html>