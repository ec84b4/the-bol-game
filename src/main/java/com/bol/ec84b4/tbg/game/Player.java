package com.bol.ec84b4.tbg.game;

/**
 * Player class, responsible for retrieving player's input
 */
public interface Player {

    int choosePit();

}