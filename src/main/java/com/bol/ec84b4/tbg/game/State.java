package com.bol.ec84b4.tbg.game;

/**
 * defines the current state of the game
 */
public class State {

    /**
     * defines the player whose turn it is it play.
     * 0 - for player 1
     * 1 - for player 2
     * by default game starts where its the second player's turn
     */
    private int currentPlayer;

    private Data[] players = new Data[2];

    /**
     * @param pitCount       number of pits in front of each player excluding the personal pit
     * @param stonePerPit    number of stones that each pit contains at the start of the game
     * @param startingPlayer which player should start first
     */
    public State(int pitCount, int stonePerPit, int startingPlayer) {
        currentPlayer = startingPlayer;
        players[0] = new Data(0, pitCount, stonePerPit);
        players[1] = new Data(0, pitCount, stonePerPit);
    }

    /**
     * updates the current state of the game by the chosen pit of the player whose turn it is
     *
     * @param pitIndex the pit chosen by the player as index
     * @return true if game has ended false if game can continue
     */
    public boolean turn(int pitIndex) {
        doTurn(pitIndex);

        currentPlayer = opponentPlayer();

        return isGameFinished();
    }

    private void doTurn(int pitIndex) {
        Data playerData = players[currentPlayer];
        Data opponentData = players[opponentPlayer()];

        int stonesInPit = playerData.stones(pitIndex);

        //this might cause the game to go into a loop if both players keep-
        //-choosing an empty pit (game state doesn't change)
        if (stonesInPit == 0)
            return;

        playerData.stones(pitIndex, 0);

        int pitCount = playerData.pitCount();

        //pointer to pit being modified
        int i = pitIndex;

        while (stonesInPit != 0) {

            i++;

            if (i > pitCount)
                i -= pitCount + 1;

            if (i == pitCount) {
                playerData.vault(playerData.vault() + 1);
            } else {
                playerData.stones(i, playerData.stones(i) + 1);
            }

            stonesInPit--;
        }

        if (i >= pitCount) i = 0;

        //the last pit has one stone in it, meaning it was empty before the distribution of the stones
        if (playerData.stones(i) == 1) {
            //opponent pit index
            int oi = pitCount - 1- i;

            //takes the stones of the opponent
            int gainedStones = opponentData.stones(oi);

            //empties opponent's pit
            opponentData.stones(oi, 0);

            //add the single stone from player's pit
            gainedStones += 1;

            //empty player's pit
            playerData.stones(i, 0);

            //add the stones into player's vault
            playerData.vault(playerData.vault() + gainedStones);
        }
    }

    /**
     * @return the winner of the game 0-> player1  1-> player2
     */
    public int winner() {
        if (!isGameFinished()) throw new IllegalStateException("game has not finished yet!");

        int[] points = new int[2];

        for (int i = 0; i < players.length; i++) {
            Data data = players[i];

            if (!data.allPitsEmpty()) {
                for (int j = 0; j < data.pitCount(); j++) {
                    data.vault(data.vault() + data.stones(j));
                    data.stones(j, 0);
                }
            }

            points[i] = data.vault();
        }

        return points[0] > points[1] ? 0 : 1;
    }

    public Data player1() {
        return players[0];
    }

    public Data player2() {
        return players[1];
    }

    /**
     * @return returns the current player for this turn 0-> player1  1-> player2
     */
    public int currentPlayer() {
        return currentPlayer;
    }

    /**
     * @return the index of the player that this turn is not his 0-> player1  1-> player2
     */
    public int opponentPlayer() {
        return currentPlayer == 0 ? 1 : 0;
    }

    private boolean isGameFinished() {
        return player1().allPitsEmpty() || player2().allPitsEmpty();
    }

    /**
     * data class representing each player's state data
     */
    public class Data {
        /**
         * player's personal pit
         */
        private int vault;

        /**
         * game pits of the player
         */
        private int[] pits;

        Data(int vault, int pitCount, int stonePerPit) {
            this.vault = vault;

            pits = new int[pitCount];
            for (int i = 0; i < pitCount; i++)
                pits[i] = stonePerPit;
        }

        public int vault() {
            return vault;
        }

        public void vault(int value) {
            this.vault = value;
        }

        public int[] pits() {
            return pits;
        }

        public int stones(int index) {
            return pits[index];
        }

        public void stones(int index, int value) {
            pits[index] = value;
        }

        public int pitCount() {
            return pits.length;
        }

        public boolean allPitsEmpty() {
            for (int s : pits) {
                if (s != 0) return false;
            }

            return true;
        }
    }
}
