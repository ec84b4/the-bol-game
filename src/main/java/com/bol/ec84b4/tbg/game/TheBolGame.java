package com.bol.ec84b4.tbg.game;

/**
 * The Bol Game
 * <p>
 * requires two players to be played
 * player with the most stones wins the game and the game is finished when either side runs out of stones
 */
public class TheBolGame {
    private State state;
    private Player[] players;
    private boolean finished;

    public TheBolGame(State state, Player[] players) {
        this.state = state;
        this.players = players;
    }

    public int start() {
        while (true) {
            int chosenPit = players[state.currentPlayer()].choosePit();

            boolean finished = state.turn(chosenPit);

            if (finished) {
                this.finished = true;
                return state.winner();
            }
        }
    }

    public State state() {
        return state;
    }

    public Player[] players() {
        return players;
    }

    public boolean finished() {
        return finished;
    }
}