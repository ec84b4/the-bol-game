package com.bol.ec84b4.tbg;

import com.bol.ec84b4.tbg.cli.CliBolGame;
import com.bol.ec84b4.tbg.cli.CliPlayer;
import com.bol.ec84b4.tbg.game.State;
import com.bol.ec84b4.tbg.game.TheBolGame;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.Scanner;

@SpringBootApplication
public class Universe extends SpringBootServletInitializer {

    private static Universe instance = new Universe();

    public static Universe instance() {
        return instance;
    }

    public static void main(String[] args) {
        if (args != null && args.length > 0 && args[0].equals("cli")) {
            new CliBolGame(new Scanner(System.in), System.out, newGame()).start();
        } else {
            SpringApplication.run(Universe.class, args);
        }
    }

    public static TheBolGame newGame() {
        State state = new State(6, 6, 1);

        CliPlayer[] players = new CliPlayer[]{new CliPlayer(0), new CliPlayer(1)};

        return new TheBolGame(state, players);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Universe.class);
    }
}
