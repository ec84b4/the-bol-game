package com.bol.ec84b4.tbg.cli;

import com.bol.ec84b4.tbg.game.Player;

public class CliPlayer implements Player {
    private CliBolGame cli;
    /**
     * defines whether this is the first player or the second
     */
    private int index;

    public CliPlayer(int index) {
        this.index = index;
    }

    public void attachCli(CliBolGame cli) {
        this.cli = cli;
    }

    @Override
    public int choosePit() {
        cli.printGame();

        cli.out().print(String.format("Player %s choose pit: ", index + 1));

        return cli.in().nextInt();
    }
}