package com.bol.ec84b4.tbg.cli;

import com.bol.ec84b4.tbg.game.State;
import com.bol.ec84b4.tbg.game.TheBolGame;

import java.io.PrintStream;
import java.util.Scanner;

public class CliBolGame {
    private Scanner in;
    private PrintStream out;
    private TheBolGame game;

    public CliBolGame(Scanner in, PrintStream out, TheBolGame game) {
        this.in = in;
        this.out = out;
        this.game = game;
        ((CliPlayer) game.players()[0]).attachCli(this);
        ((CliPlayer) game.players()[1]).attachCli(this);
    }

    public int start() {
        out.println("Welcome to The Bol Game.");

        int winner = game.start();

        out.println(String.format("\n\nHooorayyy Player %s is the winner", winner));

        return winner;
    }

    public void printGame() {
        out.print("\033[H\033[2J");
        out.flush();

        State state = game.state();

        State.Data p1 = state.player1();
        State.Data p2 = state.player2();

        out.println("Player 1:");
        out.println("Vault   " +
                "Pit#5   " +
                "Pit#4   " +
                "Pit#3   " +
                "Pit#2   " +
                "Pit#1   " +
                "Pit#0   ");
        out.println(String.format("%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     ",
                formatted(p1.vault()),
                formatted(p1.pits()[5]),
                formatted(p1.pits()[4]),
                formatted(p1.pits()[3]),
                formatted(p1.pits()[2]),
                formatted(p1.pits()[1]),
                formatted(p1.pits()[0])
        ));

        out.println();

        out.println("Pit#0   " +
                "Pit#1   " +
                "Pit#2   " +
                "Pit#3   " +
                "Pit#4   " +
                "Pit#5   " +
                "Vault   ");
        out.println(String.format("%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     " +
                        "%s     ",
                formatted(p2.pits()[0]),
                formatted(p2.pits()[1]),
                formatted(p2.pits()[2]),
                formatted(p2.pits()[3]),
                formatted(p2.pits()[4]),
                formatted(p2.pits()[5]),
                formatted(p2.vault())
        ));
        out.println("Player 2:");

        out.println();
    }

    public Scanner in() {
        return in;
    }

    public PrintStream out() {
        return out;
    }

    private String formatted(int value) {
        return "  " + value;
    }
}
