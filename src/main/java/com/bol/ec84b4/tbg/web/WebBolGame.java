package com.bol.ec84b4.tbg.web;


import com.bol.ec84b4.tbg.game.Player;
import com.bol.ec84b4.tbg.game.State;
import com.bol.ec84b4.tbg.game.TheBolGame;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class WebBolGame {
    private static TheBolGame game;

    @RequestMapping({"/", "/the-bol-game.jsp"})
    public String home(Map<String, Object> model) {
        if (game != null && game.finished())
            game = null;

        if (game == null) {
            State state = new State(6, 6, 1);
            Player[] players = new Player[]{new WebPlayer(), new WebPlayer()};
            game = new TheBolGame(state, players);
            game.start();
        }

        model.put("game", game);

        return "the-bol-game";
    }
}
