package com.bol.ec84b4.tbg.web;

import com.bol.ec84b4.tbg.game.Player;

public class WebPlayer implements Player {
    private int pick;

    @Override
    public int choosePit() {
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return pick;
    }

    public void pick(int pit) {
        pick = pit;
        synchronized (this) {
            notifyAll();
        }
    }
}
